function [A_line,zs_A_line] = A_line(x_A_line)
% function [A_line,zs_A_line] = A_line(x_A_line)
% Miklos Gyongy 2015
% function to return A-line given x interrogating position x_A_line [m]

field_init(-1);

fs = 100e6;
% radius = 10e-3;
% focal_radius = 20e-3;
radius = 10e-3;
focal_radius = 20e-3;
%ele_size = 1e-3; 
ele_size = radius/20; 

Th_tx = xdc_concave(radius,focal_radius,ele_size);
Th_rx = xdc_concave(radius,focal_radius,ele_size);

t = 0:(1/fs):1e-6;
Nt = length(t);
f = 5e6;
imp = hann(Nt)'.*sin(2*pi*f*t);
xdc_impulse(Th_tx,imp);

zs_A_line = 0:.1e-3:30e-3;
c = 1540;
t_im = 2* zs_A_line / c;

points = [...
          0      0 20e-3;...
          0      500e-6 19e-3;...
          500e-6 0 22e-3;...
          153e-6 0 21e-3;...
          ]; % scatterer positions
% points = [points; zeros(19,1) zeros(19,1) (1:19)'*1e-3];
amplitudes = 0*points + 1; % scatterer amplitude

points_rel = points;
points_rel(:,1) = points_rel(:,1) - x_A_line;
%disp(points_rel);
[scat, start_time] = calc_scat(Th_tx, Th_rx, points_rel, amplitudes);
dur_scat = (length(scat)-1)/fs; % duration of recording
t_scat = start_time + (0:(1/fs):dur_scat); % scattering recording time
A_line = interp1(t_scat,scat,t_im,'linear',0); % interpolate in time to get required data for image
