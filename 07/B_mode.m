% %% Impulse function
fs = 100e6;
t = 0:(1/fs):1e-6;
Nt = length(t);
f = 5e6;
imp = hann(Nt)'.*sin(2*pi*f*t);

figure
subplot(211)
plot(sin(2*pi*f*t))
subplot(212)
plot(imp)

% %% A-line and envelope
[A_rf,zs_A_rf] = A_line(0);
figure;
hh(1) = subplot(211);
plot(zs_A_rf,A_rf);
hh(2) = subplot(212);
plot(zs_A_rf,abs(hilbert(A_rf')));
linkaxes(hh,'xy');

%% B-mode image

%
% WRITE FOR LOOP HERE TO GET B-MODE IMAGE
%

%
% LOOK AT IMAGE DISPLAY OPTIONS
%
