function percentcomplete(num,N)

if ~exist('N','var')
    fprintf('\b\b\b%3.0f',100*num);
else
    if num==1
        fprintf('   ');
    else
        fprintf('\b\b\b%3.0f',100*num/N);
    end
end