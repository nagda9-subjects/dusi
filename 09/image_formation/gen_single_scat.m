function [data,t_seek] = gen_single_scat(z_foc,xz_scat)

if ~exist('z_foc','var')
    z_foc = 20e-3;
end

if ~exist('xz_scat','var')
    xz_scat = [0 20e-3];
end

%% Set transducer parameters
 
f0=4.7e6; % Transducer center frequency [Hz]
fs = 50e6; %sampling frequency
Nch = 64; %number of active elements
Nx = 192; %number of elements
dx= 245e-6; % space between elements (m)
dh=6e-3; % Height of element [m]
dk=0; % Kerf [m] (dead space between elements)
dw = dx-dk; %width of element
D = (Nx-1)*dx; %size of aperture (m)
sub_D = (Nch-1)*dx; %size of sub_aperture (m) - active channels
focus_elev = 20e-3;% elevation focus (m)

c0 = 1490;%standard sound speed of the one expected to be the real value in the medium

try field_init(-1); end %#ok<TRYNC>
set_sampling(fs);
set_field('c',c0);

%% Set impulse response and excitation

cycles = 2;% cycles per excitation

impulse_response=sin(2*pi*f0*(0:1/fs:cycles/f0));
impulse_response=impulse_response.*hanning(max(size(impulse_response)))';

excitation=sin(2*pi*f0*(0:1/fs:2/f0));

%% Scatterer position
x0 = xz_scat(1);
z0 = xz_scat(2);

%% Set parameters for beamforming

ThTx = xdc_focused_array(Nch,dw,dh,dk,focus_elev,1,10,...
    [0 0 z_foc]);% init. Tx transducer
ThRx = xdc_focused_array(Nch,dw,dh,dk,focus_elev,1,10,...
    [0 0 100]);% init. Rx transducer
xdc_impulse (ThTx, impulse_response);
xdc_impulse (ThRx, impulse_response);
xdc_excitation (ThTx, excitation);

t_seek = 0:(1/fs):(60e-6);
Nt = length(t_seek);
%data = zeros(Nt,64,192);
data = zeros(64,Nt,192);

for Iline = 1:Nx,
    x=(Iline-1)*dx; % position of A-line (m)
    xshift= min(max(0,x-sub_D/2),D-sub_D);%shift of active aperture (m)
   % ThTx = xdc_focused_array(Nch,dw,dh,dk,focus_elev,1,10,[-x+xshift 0 z_foc]);% init. Tx transducer
    x_rel = x0 + (D/2 - xshift - sub_D/2); % lateral position of scatterer compared to the active aperture
    [data_tmp,t0]=calc_scat_multi (ThTx, ThRx, [x_rel 0 z0],1); %channel data for each recieve element
    t_tmp = (0:(length(data_tmp)-1))/fs + t0;
    data_interp_tmp = interp1(t_tmp,data_tmp,t_seek,'linear',0); % size: [Nt,64]
    data(:,:,Iline) = data_interp_tmp';
end
    