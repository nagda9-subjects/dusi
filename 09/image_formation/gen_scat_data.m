function gen_scat_data(mode,z_shift,Cs)
% new function to generate data for speeds of sound in order to use for
% first two figures
% Miklos Gyongy October 2013
% code taken from one_scat_3, one_scat_syn
% three modes: 1: fixed focus, 2: synthetic aperture, 3: infinite focus 
% Cs: speeds of sound

%% default input parameters: speeds of sound to use, beamforming mode
if ~exist('mode','var')
    mode = 3; % infinite focus
end

if ~exist('z_shift','var')
    z_shift = 0;
end
z_shift = z_shift/1e3; % turn into meters

if ~exist('Cs','var'),
    Cs = 1240:10:1740;  % the speeds of sound used in the figures
%    Cs = [1290 1490 1690];   % the three speeds of sound highlighted 
end
Nc = length(Cs);


%% Set transducer parameters
 
f0=4.7e6; % Transducer center frequency [Hz]
fs = 50e6; %sampling frequency
Nch = 64; %number of active elements
Nx = 192; %number of elements
dx= 245e-6; % space between elements (m)
dh=6e-3; % Height of element [m]
dk=0; % Kerf [m] (dead space between elements)
dw = dx-dk; %width of element
D = (Nx-1)*dx; %size of aperture (m)
sub_D = (Nch-1)*dx; %size of sub_aperture (m) - active channels
focus_elev = 20e-3;% elevation focus (m)

c0 = 1490;%standard sound speed of the one expected to be the real value in the medium

try field_init(-1); end %#ok<TRYNC>
set_sampling(fs);
set_field('c',c0);

%% Set impulse response and excitation

cycles = 2;% cycles per excitation

impulse_response=sin(2*pi*f0*(0:1/fs:cycles/f0));
impulse_response=impulse_response.*hanning(max(size(impulse_response)))';

excitation=sin(2*pi*f0*(0:1/fs:2/f0));

%% Scatterer position
x0 = 0;
z0 = 20e-3+z_shift;

%% Set parameters for beamforming

dz = 15e-6; %axial pixel height (m)
zs = (z0-1e-3):dz:(z0+4e-3);% + (-10e-3:dz:10e-3); % axial pixel positions
Nz = length(zs); %number of axial pixels


%% Calculate images

switch(mode)
    case 1 % fixed focus
        ThTx = xdc_focused_array(Nch,dw,dh,dk,focus_elev,1,10,...
            [0 0 20e-3]);% init. Tx transducer
        ThRx = xdc_focused_array(Nch,dw,dh,dk,focus_elev,1,10,...
            [0 0 100]);% init. Rx transducer
        xdc_impulse (ThTx, impulse_response);
        xdc_impulse (ThRx, impulse_response);
        xdc_excitation (ThTx, excitation);
        ims = INT_im(x0,z0,dx,zs,c0,Cs,ThTx,ThRx,fs);
        Sfile = sprintf('DATA_%dmmSCAT_ffBF_%02dCs.mat',...
            round(z_shift*1e3),Nc);
    case 2 % synthetic aperture
        % should use Nx (same as Nlmn) instead of Nch???
        Th = xdc_focused_array(Nx,dw,dh,dk,focus_elev,1,10,...
            [0 0 100]);% init. Tx and Rx transducer
        xdc_impulse (Th, impulse_response);
        xdc_excitation (Th, excitation);
        ims = INT_im_sa(x0,z0,dx,zs,c0,Cs,Th,Th,fs);
        Sfile = sprintf('DATA_%dmmSCAT_saBF_%02dCs.mat',...
            round(z_shift*1e3),Nc);
    otherwise % infinite focus
        Th = xdc_focused_array(Nch,dw,dh,dk,focus_elev,1,10,...
            [0 0 100]);% init. Tx and Rx transducer
        xdc_impulse (Th, impulse_response);
        xdc_excitation (Th, excitation);
        ims = INT_im(x0,z0,dx,zs,c0,Cs,Th,Th,fs);
        Sfile = sprintf('DATA_%dmmSCAT_ifBF_%02dCs.mat',...
            round(z_shift*1e3),Nc);
end


%% Save lateral pixel positions
xs=dx*(0:(Nx-1));
xs=xs-(xs(end)-xs(1))/2; % lateral pixel positions (m)

%% Save data to file
% one_scats = cell(Nc,1);
% for i=1:Nc
%     one_scats{i} = ims{i}(401:601,51:142);
% end
% save(['one_scat_inf_' num2str(dif) '.mat'],'one_scats','Cs');
%end

D;sub_D;ims;xs;Nz;%#ok<VUNUS> % avoid warning messages elsewhere

%imagesc(xs,zs,20*log10(abs(hilbert(ims{2})))+430,[-40 0]), colorbar
Sfile = [Sfile(1:(end-4)) '_mod.mat'];
save(Sfile,'ims','xs','zs','Cs');
fprintf('\nSaved file %s\n',Sfile);

%save(Sfile,'ims','xs','zs','Cs');
%fprintf('\n*Not* saved file %s\n',Sfile);


%% Internal functions

function ims = INT_im(x0,z0,dx,zs,c0,Cs,ThTx,ThRx,fs)

Nch = 64;
Nx = 192;
Nz = length(zs);
Nc = length(Cs);

D = (Nx-1)*dx; %size of aperture (m)
sub_D = (Nch-1)*dx; %size of sub_aperture (m) - active channels

ims = cell(Nc,1);
for i=1:Nc
    ims{i} = zeros(Nz,Nx); % initializing beamformed images
end

%for N = 1: Nx % calculating each A-line 1:192
for N = 96
    x=(N-1)*dx; % position of A-line (m)
    xshift= min(max(0,x-sub_D/2),D-sub_D);%shift of active aperture (m)
    xs_active = xshift + linspace(0,sub_D,Nch); % positions of active elements (m)

    x_rel = x0 + (D/2 - xshift - sub_D/2); % lateral position of scatterer compared to the active aperture
    
    [data,t0]=calc_scat_multi (ThTx, ThRx, [x_rel 0 z0],1); %channel data for each recieve element

    Ndata = size(data,1);% number of data points for each channel

    Ishift = Ndata*(0:(Nch-1)); % index shift to access right channel

 %/   for i = 1:Nc % for each sound speed
    for i = 26 % for each sound speed
        Aline = zeros(Nz,1);%initializing A-line

        c = Cs(i);
        zs_mod=(c/c0)*zs; %taking depths that correspond to 40-60 mm range when using standard c 1540 m/s
        for I = 1:Nz %for each depth
            z = zs_mod(I); % current depth
            Is = round(  ((z + sqrt(z^2 + (abs(xs_active-x)).^2))/c-t0) *fs ); % propagation indices there and back to each element
            Is = Is + 61; % to account for field II propensity for have t0 for beginning of pulse
            Is_inrange=INT_inrange(Is,Ndata);%
            Is = max(1, min(Ndata,Is) ); % enforce lower and upper limits
            Is = Is + Ishift;  % index shift to access right channel
 %          if N==64,
            cla, imagesc(data), hold on, plot(1:64,Is-Ishift);
            Stitle = sprintf('x = %.1f mm, z = %.2f mm',x_rel*1e3,z*1e3);
            title(Stitle);
    %        pause
  %          end
            Aline(I) = sum(data(Is).*Is_inrange);%sum only channels with signal in range
        end

        ims{i}(:,N) = Aline;
    end
    INT_percentcomplete(N,Nx); % indicate percentage completion of loop

end

% synthetic aperture bf *** FIX THIS
function ims = INT_im_sa(x0,z0,dx,zs,c0,Cs,ThTx,ThRx,fs)

Nc = length(Cs);
Nch = 64; %number of active elements
Nlmn = 192; %number of elements

%[scat, start_time] = calc_scat_all (Th1, Th2, points, amplitudes, dec_factor);
[data,t0] = calc_scat_all(ThTx,ThRx,[x0 0 z0],1,1);
Ndata = size(data,1);

%Set parameters for beamforming
%rows = 401:601;
columns = 51:142;

%dz = 20e-6; %axial pixel height (m)
%zs = 50e-3 + (-10e-3:dz:10e-3); % axial pixel positions

%zs = zs(rows);
%dx= 245e-6; % space between elements (m)
%dh=5e-3; % Height of element [m]
%dk=0.1e-3; % Kerf [m] (dead space between elements)
%dw = dx-dk; %width of element
D = (Nlmn-1)*dx; %size of aperture (m)
xlmn = -D/2:dx:D/2;% element positions (m)

xs = xlmn(columns);

Nz = length(zs); %number of axial pixels
Nx = length(xs);

ims = cell(Nc,1);
for i=1:Nc
    ims{i} = zeros(Nz,Nx); % initializing beamformed images
end

%Receive beamforming - calculating images

for N = 1: Nx % calculating each A-line 1:192
    x= xs(N); % position of A-line (m)
    Ishift= min(max(0,N+columns(1)-1-Nch/2),Nlmn-Nch);% index shift of active aperture
    Ind = Ishift+(1:Nch); %indexes of active elements
    xs_recieve = xlmn(Ind); % positions of active elements (m)

    shift_R = Ndata*(0:(Nlmn-1));%index shift to access correct recieve element
    shift_T = shift_R*Nlmn;%index shift to access correct transmit element

    for i = 1:Nc
        Aline = zeros(Nz,1);%initializing A-line
        
        c = Cs(i);
        zs_mod=(c/c0)*zs; %taking depths that correspond to 40-60 mm range when using standard c 1540 m/s

        for I = 1:Nz %for each depth
            z = zs_mod(I); % current depth
            
            for T = Ind % for each transmit element to the A-line
                x_transmit = xlmn(T);
                Is = round( ( (sqrt(z^2+(x_transmit-x)^2)+sqrt(z^2+(xs_recieve-x).^2))/c -t0) *fs); % propagation indices there and back to each element
                %Is_inrange=INT_inrange(Is,Ndata);%
                Is = max(1, min(Ndata,Is) ); % enforce lower and upper limits
                Is = Is + shift_T(T) + shift_R(Ind);  % index shift to access right channel
                %Aline(I) = Aline(I)+sum(data(Is).*Is_inrange);%sum only channels with signal in range
                try
                Aline(I) = Aline(I)+sum(data(Is));%sum only channels with signal in range
                catch %#ok<CTCH>
                   save SA_error_log 
                   error('error');
                end
            end
        end
       
        ims{i}(:,N) = Aline;
    end
    INT_percentcomplete(N,Nx); % indicate percentage completion of loop
    
end



function vec = INT_inrange(Is,Ndata)

vec_1 = Is>1;
vec_2 = Is<Ndata;
vec=vec_1.*vec_2;


function INT_percentcomplete(num,N)
% indicate completion of task in terms of percentage

if ~exist('N','var')
    fprintf('\b\b\b%3.0f',100*num);
else
    if num==1
        fprintf('   ');
    else
        fprintf('\b\b\b%3.0f',100*num/N);
    end
end

