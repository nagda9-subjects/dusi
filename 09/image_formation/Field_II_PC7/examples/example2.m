% Example of use of the new Field II program running under Matlab
%
% This example shows how a linear array B-mode system scans an image
%
% This script assumes that the field_init procedure has been called
%
% Example by Joergen Arendt Jensen, Version 1.1, June 3, 1998.
% Generate the transducer apertures for send and receive
f0=3e6; % Transducer center frequency [Hz]
fs=50e6; % Sampling frequency [Hz]
c=1540; % Speed of sound [m/s]
width=1/1000; % Width of element
element_height=5/1000; % Height of element [m]
kerf=0.1/1000; % Kerf [m]
focus=[0 0 70]/1000; % Fixed focal point [m]
N_elements=64;
% Set the sampling frequency
set_sampling(fs);
% Generate aperture for emission
emit_aperture = xdc_linear_array (N_elements, width, element_height, kerf, 1, 1,focus);
% Set the impulse response and excitation of the emit aperture
impulse_response=sin(2*pi*f0*(0:1/fs:2/f0));
impulse_response=impulse_response.*hanning(max(size(impulse_response)))';
xdc_impulse (emit_aperture, impulse_response);
excitation=sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation (emit_aperture, excitation);
% Generate aperture for reception
receive_aperture = xdc_linear_array (N_elements, width, element_height, kerf, 1, 1,focus);
% Set the impulse response for the receive aperture
xdc_impulse (receive_aperture, impulse_response);
% Load the computer phantom
[phantom_positions, phantom_amplitudes] = cyst_phantom(1000);
% Do linear array imaging
no_lines=40; % Number of A-lines in image
sector=width*25; % Size of image sector
d_x=sector/no_lines; % Increment for image
z_focus=70/1000;
% Pre-allocate some storage
image_data=zeros(800,no_lines);
x= -sector/2;
for i=1:no_lines
i
% Set the focus for this direction
xdc_center_focus (emit_aperture, [x 0 0]);
xdc_focus (emit_aperture, 0, [x 0 z_focus]);
xdc_center_focus (receive_aperture, [x 0 0]);
xdc_focus (receive_aperture, 0, [x 0 z_focus]);
% Calculate the received response
[v, t1]=calc_scat(emit_aperture, receive_aperture, phantom_positions, phantom_amplitudes);
% Store the result
image_data(1:max(size(v)),i)=v;
times(i) = t1;
% Steer in another direction
x = x + d_x;
end
% Free space for apertures
xdc_free (emit_aperture)
xdc_free (receive_aperture)
% Adjust the data in time and display it as
% a gray scale image
min_sample=min(times)*fs;
max_sample=max(times)*fs;
[n,m]=size(image_data);
n=n+(max_sample-min_sample);
for i=1:no_lines
rf_env=abs(hilbert([zeros(times(i)*fs-min_sample,1); image_data(:,i)]));
env(1:max(size(rf_env)),i)=rf_env;
end
% Do logarithmic compression
env=env/max(max(env));
env=log(env+0.1);
env=env-min(min(env));
env=64*env/max(max(env));
image(env)
colormap(gray(64))
