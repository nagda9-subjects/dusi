clear
close all

try
field_init;
end

f0=3e6; % Transducer center frequency [Hz]
fs=100e6; % Sampling frequency [Hz]
c=1540; % Speed of sound [m/s]
element_height=5e-3; % Height of element [m]
kerf=0.1e-3; % Kerf [m]
focus=[0 0 70e-3]; % Fixed focal point [m]
Nlmn=256;
D = 38e-3;
width = D/Nlmn;
emit_aperture = xdc_linear_array (Nlmn, width, element_height, kerf, 1, 1,focus);
impulse_response=sin(2*pi*f0*(0:1/fs:2/f0));
impulse_response=impulse_response.*hanning(max(size(impulse_response)))';
xdc_impulse (emit_aperture, impulse_response);
excitation=sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation (emit_aperture, excitation);
% Generate aperture for reception
receive_aperture = xdc_linear_array (Nlmn, width, element_height, kerf, 1, 1,focus);
% Set the impulse response for the receive aperture
xdc_impulse (receive_aperture, impulse_response);
% Load the computer phantom
phantom_positions(1,:) = [-5e-3 0 35e-3];
phantom_positions(2,:) = [0 0 70e-3];
phantom_positions(3,:) = [5e-3 0 75e-3];
phantom_amplitudes = ones(size(phantom_positions,1),1);
%[phantom_positions, phantom_amplitudes] = cyst_phantom(500);

% Do linear array imaging
Nlines=64; % Number of A-lines in image
sector=D; % Size of image sector
d_x=sector/Nlines; % Increment for image

% Pre-allocate some storage
image_data=zeros(800,Nlines);
x= -sector/2;

xs = from_to_N(-D/2,D/2,Nlines);

for i=1:Nlines
    i;
    xc = xs(i);
    z_focus = 68e-3;
    % Set the focus for this direction
    xdc_center_focus (emit_aperture, [xc 0 0]);
    xdc_focus (emit_aperture, 0, [xc 0 z_focus]);
%  xdc_dynamic_focus(emit_aperture,0,0,0);
    xdc_center_focus (receive_aperture, [xc 0 0]);
  %  xdc_focus (receive_aperture, 0, [x 0 z_focus]);
  xdc_dynamic_focus(receive_aperture,0,0,0);
    % Calculate the received response
    [v, t1]=calc_scat(emit_aperture, receive_aperture, phantom_positions, phantom_amplitudes);
    
    % Store the result
    image_data(1:max(size(v)),i)=v;
    times(i) = t1;
    % Steer in another direction
    x = x + d_x;
    percentcomplete(i/Nlines);
 %   pause
end
fprintf('First stage complete\n\n\n\n');
% Free space for apertures
xdc_free (emit_aperture)
xdc_free (receive_aperture)
% Adjust the data in time and display it as
% a gray scale image
min_sample=min(times)*fs;
max_sample=max(times)*fs;
[n,m]=size(image_data);
n=n+(max_sample-min_sample);
% for i=1:Nlines
% rf_env=abs(hilbert([zeros(times(i)*fs-min_sample,1); image_data(:,i)]));
% env(1:max(size(rf_env)),i)=rf_env;
% %percentcomplete(i/Nlines);
% pause
% end
% % Do logarithmic compression
% env=env/max(max(env));
% env=log(env+0.1);
% env=env-min(min(env));
% env=64*env/max(max(env));
% image(env)
% colormap(gray(64));

env = abs(hilbert(image_data));
env=env/max(max(env));
env=log(env+0.1);
env=env-min(min(env));
env=64*env/max(max(env));
image(env)

imagesc([-D D]/2,[0 100e-3],abs(hilbert(image_data)))
imagesc([-D D]/2,[0 100e-3],env)
axis image
