try
field_init;
catch
end

% Nsubx,Nsuby: elements subdivided in x or y direction;
% all dimensions in metres;
% kerf outside element width
Nlmn = 128;
width = 38e-3/Nlmn;
height = 10e-3;
kerf = 0;
Nsubx = 1;
Nsuby = 1;
foc_xyz = [0 0 70];

XarrayTx = xdc_linear_array(Nlmn,width,height,kerf,Nsubx,Nsuby,foc_xyz);
XarrayRx = xdc_linear_array(Nlmn,width,height,kerf,Nsubx,Nsuby,foc_xyz);

fs = 100e6;
set_sampling(fs);
f0 = 7.5e6;

Xtx = sin(2*pi*f0*(0:1/fs:2/f0));
xdc_excitation(XarrayTx,Xtx);

[phantom_positions, phantom_amplitudes] = cyst_phantom(1000);


% each row corresponds to set of apodizations from given time
% so that for multi-row array, apodization setting is row vector
% "reading out" apodization values row by row
% here, at 10 us, apodization changes from uniform to hanning
apo0 = ones(1,Nx*Ny);
apo1 = reshape(hanning(Nx)*ones(1,Ny),1,Nx*Ny);
xdc_apodization(Th,[0 10e-6], [apo0; apo1]);

field_end