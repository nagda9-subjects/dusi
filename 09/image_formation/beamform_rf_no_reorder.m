function [im_out,im] = beamform_rf_no_reorder(RF,xs_im,zs_im,Ninterp,Nlmn,Bdynapod) 
% function [im_out,im] = beamform_rf_xs_zs_Ninterp_Nlmn_Bdynapod(RF,xs_im,zs_im,Ninterp,Nlmn,Bdynapod)  
% Miklos Gyongy 2015
%
% rf:     pre_beamformed RF data
% xs_im:  transverse locations for imaging 
% zs_im:  axial locations for imaging
% Nlmn:   number of (maximum) elements to use
% Bdynapod: vary aperture progressively, reaching maximum at zs_im(end)
    
%begin=3e-3;
begin=0;
zs_im = zs_im+begin;

xs_im_orig = xs_im;
zs_im_orig = zs_im;

dx = xs_im(2)-xs_im(1);
dz = zs_im(2)-zs_im(1);
xs_im = xs_im(1):(dx*Ninterp):xs_im(end);
zs_im = zs_im(1):(dz*Ninterp):zs_im(end);

fs=50e6; % sampling frequency
c = 1482;
Iadj = begin*2/1540*fs;
zadj = (Iadj/fs) * (1482/2);

act_D = 47e-3/3;
% act_D = Nlmn...
% 
% if act_D > (47e-3)/3
%     act_D = (47e-3)/3; %if beamforming with 64 or more elements, 64 will be the max out of 192
% end

Ndata = size(RF,2); % number of data points for each channel
Nch= size(RF,1); % number of active channels (64)
dx= 245e-6; % space between elements (m)

D = 192*dx; %size of aperture (m)
sub_D0 = (Nch-1)*dx; %size of sub_aperture (m) - active channels
%sub_D0 = sub_D;
%sub_D = sub_D0;

xs_array = linspace(-D/2,D/2,192); % position of array elements
xs_im = xs_im(1):(xs_array(2)-xs_array(1)):xs_im(end);

% if ~exist('act_D','var'), act_D = (47e-3)/3;
%     elseif isempty(act_D), act_D = (47e-3)/3;
% end

Nx = length(xs_im);
Nz=size(zs_im,2);

Ishift = Ndata*(0:(Nch-1)); % index shift to access right channel

%RF=rf_reorder(RF); % reorder pre-beamforming appropriately
im= zeros(Nz,Nx); % initialize image

%Iarray = 1:192;
%x_shift = 

for Ix=1:Nx % calculating the A-lines for all xs
%for N = 96
    x_im = xs_im(Ix); % x position of current image
    [~,Ic] = min(abs(x_im-xs_array)); % find index of closest element
    IcL = max(Ic-1,1); % index of closest element to left
    IcR = min(Ic+1,192); % index of closest element to right
    xcL = xs_array(IcL); % position of closest element to left
    xcR = xs_array(IcR); % position of closest element to right
 
    LLLIL = min(192-64,max(1,IcL-32)); % index of leftmost element
    LLLxL = xs_array(LLLIL); % position of leftmost element
    LLLxs_subarray = LLLxL + linspace(0,sub_D0,Nch);
    
    RRRIL = min(192-64,max(1,IcR-32)); % index of leftmost element
    RRRxL = xs_array(RRRIL); % position of leftmost element
    RRRxs_subarray = RRRxL + linspace(0,sub_D0,Nch);
    
    LLLdata = double(RF(:,:,IcL))'; % pull out relevant RF data
    RRRdata = double(RF(:,:,IcR))'; % pull out relevant RF data
    LLLAline = zeros(Nz,1); % initialize A-line
    RRRAline = zeros(Nz,1); % initialize A-line
    
%     ext_info.xL(Ix) = LLLxL;
%     ext_info.xs_subarray(Ix,:) = LLLxs_subarray;
    
    for Iz = 1:Nz % % for each depth (1 to 2730), generate Aline from channel data 
%         xs_mod = xs(abs(xc-xs)<=(act_D/2));
%         Ishift_mod = Ishift(abs(xc-xs)<=(act_D/2));
        
        z = zs_im(Iz); % current depth
       
       if Bdynapod
           sub_D = (z+zadj)/(zs_im(end)+zadj) * sub_D0 * Nlmn/Nch;
       else
           sub_D = sub_D0 * Nlmn/Nch;
       end
        
        % left approximation
        LLLact_D = min(sub_D,act_D); % active beamforming aperture
        LLLxs_subD = LLLxs_subarray(abs(x_im-LLLxs_subarray)<=(LLLact_D/2)); % x locations of active beamforming aperture
        LLLIshift_mod = Ishift(abs(x_im-LLLxs_subarray)<=(LLLact_D/2));
        LLLdists = sqrt(z^2 + (x_im-xcL).^2) + ...
                sqrt(z^2 + (x_im-LLLxs_subD).^2); % + ...
 %               - begin*2; % propagation distances
        LLLIs = round(LLLdists/c*fs-Iadj); % propagation indices there and back to each element
        LLLIs = max(1, min(Ndata,LLLIs) ); % enforce lower and upper limits
        LLLIs = LLLIs + LLLIshift_mod;  % index shift to access right channel
        LLLAline(Iz) = mean(LLLdata(LLLIs));
        LLLAline(isnan(LLLAline)) = 0; % set to zero when no data is being summed
        
        % right approximation
        RRRact_D = min(sub_D,act_D); % active beamforming aperture
        RRRxs_subD = RRRxs_subarray(abs(x_im-RRRxs_subarray)<=(RRRact_D/2)); % x locations of active beamforming aperture
        RRRIshift_mod = Ishift(abs(x_im-RRRxs_subarray)<=(RRRact_D/2));
        RRRdists = sqrt(z^2 + (x_im-xcR).^2) + ...
                sqrt(z^2 + (x_im-RRRxs_subD).^2); % + ...
%                - begin*2; % propagation distances
        RRRIs = round(RRRdists/c*fs-Iadj); % propagation indices there and back to each element
        RRRIs = max(1, min(Ndata,RRRIs) ); % enforce lower and upper limits
        RRRIs = RRRIs + RRRIshift_mod;  % index shift to access right channel
        RRRAline(Iz) = mean(RRRdata(RRRIs));
        RRRAline(isnan(RRRAline)) = 0; % set to zero when no data is being summed
        
        
        
%         if Ix==round(Nx/2), ext_info.act_D(Iz) = LLLact_D; end;
        
    end  % for I=1:Nz
    weighting = (xcR-x_im)/(xcR - xcL); % how much of LLLAline to include
%    weighting = 1;
    im(:,Ix)=weighting*LLLAline + (1-weighting)*RRRAline;
    percentcomplete(Ix,Nx);
end

im_out = interp2(xs_im,zs_im',im,xs_im_orig,zs_im_orig','spline'); % checked -- good

if nargout==0
    subplot(1,2,2), cla
%        imagesc(xs_im*1e3,zs_im*1e3,envelope(im));
        imagesc(xs_im_orig*1e3,zs_im_orig*1e3,envelope(im_out));
%     subplot(1,2,2)
%         imagesc(xs_im_orig*1e3,zs_im_orig*1e3,envelope(im_out));
end

%disp('ok');

% ext_info.x = xs_im*1e3;
% ext_info.z = zs_im*1e3;
% ext_info.fn = act_D;
% ext_info.c = c;

