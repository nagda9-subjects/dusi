z_foc = 20e-3;

RF1 = gen_single_scat(z_foc, [0 20e-3]);
RF2 = gen_single_scat(z_foc, [10e-3 20e-3]);
RF = RF1 + RF2;

%%
figure(200);

for i = 32:(size(RF,3)-31)
    imagesc(0:length(RF), 245e-3*(-31:32), RF(:,:,i));
    title(['\fontsize{64}' num2str(i)]);
    xlabel('time index');
    ylabel('channel position [mm]');
    pause(0.1);   
end