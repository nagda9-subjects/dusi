% image region
dx = 20e-6; % inter-pixel distance
xs_im = -5e-3:dx:5e-3; % transverse pixel locations
%zs_im = 17.2e-3:dx:23.7e-3; % axial pixel locations
zs_im = 0:dx:50e-3; % axial pixel locations

%z_foc = 20e-3;
z_foc = 1;

RF1 = gen_single_scat(z_foc,[3e-3 5e-3]);
RF2 = gen_single_scat(z_foc,[0e-3 20e-3]);
RF3 = gen_single_scat(z_foc,[0e-3 40e-3]);

RF = RF1 + RF2 + RF3;

Ninterp = 3; % interpolation factor
Nlmn = 64; % number of elements to beamform with
Bdynapod = true; % whether to employ dynamic receive apodization 
im_RF = beamform_rf_no_reorder(RF,xs_im,zs_im,Ninterp,Nlmn,Bdynapod);


figure;
subplot(1,2,1)
imagesc(0:length(RF),245e-3*(-31:32),RF(:,:,96))
%imagesc(0:length(RF),245e-3*(-31:32),RF(:,:,96)~=0)
xlabel('time index');
ylabel('channel position (mm)');

subplot(1,2,2)
imagesc(xs_im*1e3,zs_im*1e3,abs(hilbert(im_RF-mean(im_RF(:)))));
xlabel('transverse position (mm)');
ylabel('axial position (mm)');