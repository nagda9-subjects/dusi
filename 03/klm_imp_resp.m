clear all;
% close all;

%% characteristic acoustic impedance of different materials
% Z_fr4 = 6.64e6; % FR4

% gold
Z_au = 63.8e6; % gold
v_au = 3.24e3; % gold
l_au = 1e-6; % thickness of gold deposition 
% Z_PVDF = ; % PVDF
% Z_py = 3.16e6; % polyimide
Z_epo = 15e6; % 2.76e6; % epoxy (or 5.14e6)
Z_water = 1.5e6; % water
Z_air = 430; % air 

v_acryl = 2750; 
rho_acryl = 1.19e3; 
Z_acryl = v_acryl*rho_acryl;

%% PVDF piezo properties
%piezo.l= 28e-6; % piezo thickness [m]    52e-6
piezo.l= 56e-6; 
piezo.rho = 1.78e3; % density
piezo.c_D = 8.7e9*exp(1i*0.133 * (pi/2) );
piezo.k_t = 0.146*exp(1i*0.0362 * (pi/2) );
piezo.eta_s = .104 * exp(1i*-.023 * (pi/2) );% epsilon; dielectric, JCp53
% non-attenuative
% [piezo.c_D piezo.k_t piezo.eta_s] = deal(real(piezo.c_D),real(piezo.k_t),real(piezo.eta_s));
piezo.r = 3e-3/2; % radius

%% model set-up

% intermediate backing layers
back = [];
back(1).Z = Z_au;
back(1).l = l_au;
back(1).v = v_au;

% final semi-infinite backing layer
back(end+1).Z = Z_water; 
%back(end+1).Z = Z_air; 

% intermediate front layers
front = [];
% front(1).Z = Z_au;
% front(1).l = l_au;
% front(1).v = v_au;
l_match = piezo.l/2;
front(1).Z = Z_acryl;
front(1).l = l_match;
front(1).v = v_acryl;

% final semi-infinite front layer
% water
front(end+1).Z = Z_water;
%front(end+1).Z = Z_air; 

df = 100e3;
fs = 400e6;
f = 0:df:fs;
Nf = length(f);
tt = zeros(1,Nf); % transmit response
rr = zeros(1,Nf); % receive response

[T, R, TR, f] = kervel_klm_general(piezo,back,front,f);

TR = TR/(1i*2*pi);
NH = length(TR);
hannW=hann(NH*2);   % Hanning window
TR = TR .* (hannW(NH+1:end)');    % Hanning filter

t = linspace(0,1/df, length(TR));
tr = real(ifft(TR));

%% plot results
% figure(103), clf
figure;
subplot(1,2,1)
plot(t*1e6,tr)
xlabel('Time [us]')
title('Transmit Impulse response')
xlim([0 0.5])
subplot(1,2,2)
plot(f/1e6, abs(TR))
title('Transmit Transfer function')
xlabel('Frequency [MHz]')
title('Transfer function')
xlim([0 40])
