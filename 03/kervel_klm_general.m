function [H_t, H_r, H_p, f] = kervel_klm_general(piezo,back,front,f)
% general KLM calculator
% [H_t, H_r, H_p, f] = kervel_klm_general(piezo,back,front,f)
% transmit, receive, overall, frequency

A = pi*piezo.r^2;
%A = pi*(15e-3/2)^2;
%A = 1;

if ~exist('f','var'), f = 0:100e3:60e6; end
H_t = 0*f;
H_r = 0*f;
H_p = 0*f;
omega = 2*pi*f;

%% calculate piezo properties
% piezo.rho = 1.78e3; % density?
% piezo.c_D = complex(8.6e9,8.6e9*0.135); % compressibility
% piezo.k_t = complex(0.145,(0.145*0.091)); % eff. pelec coupling coeff, (A4)
% piezo.eta_s = 6.3*8.8542e-12*complex(1,0.256); % epsilon; dielectric, JCp53
piezo.Z = A*sqrt(piezo.c_D*piezo.rho); % crystal impedance, 3.9e6*A
piezo.v = abs(sqrt(piezo.c_D/piezo.rho)); % speed of sound in the piezo (real)
piezo.om_0 = pi*piezo.v/piezo.l; % (A4-)
piezo.tau = piezo.l/(2*piezo.v); % (A7-)

for If = 1:length(f)

om = omega(If);
om=abs(om);
p = 1i*om;
if p == 0
p = eps;
end

%% calculate electric properties

%Z_e = 50; % recvr i/p Z, (13): (Nt22.Zt-Nt12)/(Nt11-Nt21.Zt)

    % transformer turns; with current settings, id matrix
        n = 1; % turns ratio
        L = 0; % no inductance, no matching
        N_1 = [ n,  -p*L/n; 0, 1/n]; % (A1)
    
    % capacitances
        C_0 = piezo.eta_s*A/piezo.l; % (A3)
        C_prime = -C_0/(piezo.k_t^2*sinc(om/piezo.om_0)); % (A4)
        N_2 = [1, -(C_0 + C_prime)/(p*C_0*C_prime); 0, 1]; % A(2)
        %N_2(isinf(N_2)) = max(max(N_2(isnumeric(N_2))));
    
    % transformer
        psi = piezo.k_t*sqrt(pi/(piezo.om_0*C_0*piezo.Z))*sinc(om/(2*piezo.om_0)); % (A5-)
        N_3 = [ 1/psi, 0;    0, psi]; % (A5)



%% calculate back acoustic properties
% transfer matrix of lambda/4 layer (A6)
    N_5 = [cosh(p*piezo.tau), -piezo.Z*sinh(p*piezo.tau)
        -(1/piezo.Z)*sinh(p*piezo.tau), cosh(p*piezo.tau)];
    N_b = N_5;
for Iback = 1:(length(back)-1)
    Z_tmp = A*back(Iback).Z;
    l_tmp = back(Iback).l;
    v_tmp = back(Iback).v;
    tau_tmp = l_tmp/v_tmp;
            
    N_tmp = [cosh(p*tau_tmp), -Z_tmp*sinh(p*tau_tmp)
       -(1/Z_tmp)*sinh(p*tau_tmp), cosh(p*tau_tmp)];
    N_b = N_b*N_tmp;   
end

Z_b = A*back(end).Z;
    
N_equiv = N_b*[-Z_b;1];
Z_equiv = N_equiv(1)/N_equiv(2);    % transformed back impedance
N_4 = [1 0;1/Z_equiv 1];
    
    
%% calculate front acoustic properties
    N_em = N_4*N_3*N_2*N_1; % electromechanical port, Fig. 4, (10,A9)

    N_6 = [cosh(p*piezo.tau), -piezo.Z*sinh(p*piezo.tau)
        -(1/piezo.Z)*sinh(p*piezo.tau), cosh(p*piezo.tau)];

    N_t = N_6*N_em;
   for Ifront = 1:(length(front)-1)
    Z_tmp = A*front(Ifront).Z;
    l_tmp = front(Ifront).l;
    v_tmp = front(Ifront).v;
    tau_tmp = l_tmp/v_tmp;
            
    N_tmp = [cosh(p*tau_tmp), -Z_tmp*sinh(p*tau_tmp)
       -(1/Z_tmp)*sinh(p*tau_tmp), cosh(p*tau_tmp)];
    N_t = N_tmp*N_t;   
   end 
   
Z_t = A*front(end).Z;
        
%% calculate overall properties

 %   Z_e = 50; % (N_t(2,2)*Z_t-N_t(1,2))/(N_t(1,1)-N_t(2,1)*Z_t);
    Z_e = (N_t(2,2)*Z_t-N_t(1,2))/(N_t(1,1)-N_t(2,1)*Z_t);
    
    H_t(If) = 2*Z_t/(-Z_e*Z_t*N_t(2,1)+ Z_e*N_t(1,1) - N_t(1,2) +Z_t*N_t(2,2));
 %   H_r(If) = 2*Z_e/(-Z_e*Z_t*N_t(2,1) + Z_t*N_t(2,2) + N_t(1,2) +Z_e*N_t(1,1));
    H_r(If) = 2*Z_e/(-Z_e*Z_t*N_t(2,1) + Z_t*N_t(2,2) - N_t(1,2) +Z_e*N_t(1,1));
    H_p(If) = 4*Z_e*Z_t/...
        (Z_e*Z_t*N_t(2,1)-Z_e*N_t(1,1)+N_t(1,2)-Z_t*N_t(2,2))^2;

end

